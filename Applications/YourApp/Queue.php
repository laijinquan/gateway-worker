<?php
namespace app;
require_once __DIR__.'/Redis.php';

class Queue
{
    //设置Worker子进程启动时的回调函数，每个子进程启动时都会执行。
    public function onWorkerStart($worker)
    {
        //redis连接
        global $redisDb;
        $redis   = \app\Redis::GetInstance();
        $redisDb = $redis->getRedis();

        //多久实行一次
        $time_interval = 2;
        \Workerman\Lib\Timer::add($time_interval, function()use($redisDb){
            $redisDb->lpop('ren',function ($result, $redis){
                if ($result==null)
                {
                    sleep(5);
                }
                var_dump($result);
            });
        },true);
    }


    //当客户端与Workerman建立连接时(TCP三次握手完成后)触发的回调函数。
    public function onConnect($connection)
    {
    }


    //当客户端通过连接发来数据时(Workerman收到数据时)触发的回调函数
    public function onMessage($connection, $task_data)
    {
    }


    //当客户端连接与Workerman断开时触发的回调函数。
    public function onClose($connection)
    {
    }


    //当客户端的连接上发生错误时触发。
    public function onError($connection)
    {
    }
}