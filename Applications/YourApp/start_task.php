<?php
/**
 * 用于处理异步任务
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/29
 * Time: 17:24
 */

// 自动加载类
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__.'/Task.php';
require_once __DIR__.'/File.php';

use \Workerman\Worker;
use \Workerman\WebServer;

// task worker，使用Text协议
$task_worker = new Worker('Text://0.0.0.0:9502');
// task进程数可以根据需要多开一些
$task_worker->count = 5;
$task_worker->name = 'Task';

//设置日志文件
$date = date('Y-m-d',time());
$path = __DIR__."/../runtime/start$date.log";
\app\File::createFileLog($path);
Worker::$logFile = $path;



// 创建一个对象
$task = new \app\Task();

// 调用类的方法
$task_worker->onWorkerStart= array($task, 'onWorkerStart');
$task_worker->onConnect    = array($task, 'onConnect');
$task_worker->onMessage    = array($task, 'onMessage');
$task_worker->onClose      = array($task, 'onClose');
$task_worker->onError      = array($task, 'onError');

// 启用swoole的事件驱动
//Worker::$eventLoopClass = Workerman\Events\Swoole::class;


// 如果不是在根目录启动，则运行runAll方法
if(!defined('GLOBAL_START'))
{
    Worker::runAll();
}
