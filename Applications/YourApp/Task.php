<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/29
 * Time: 17:14
 */
namespace app;
require_once __DIR__.'/Mysql.php';
require_once __DIR__.'/Redis.php';
require_once __DIR__.'/Handle.php';


class Task
{
    private $handle;

    //设置Worker子进程启动时的回调函数，每个子进程启动时都会执行。
    public function onWorkerStart($worker)
    {
        //数据库了连接
//        global $mysqlDb;
//        $mysql   = \app\Mysql::GetInstance();
//        $mysqlDb = $mysql -> getMySqlDb();

        //redis连接
//        global $redisDb;
//        $redis   = \app\Redis::GetInstance();
//        $redisDb = $redis->getRedis();

        //协程
    }


    //当客户端与Workerman建立连接时(TCP三次握手完成后)触发的回调函数。
    public function onConnect($connection)
    {
        $handle = new Handle();
        $this->handle = $handle;
    }


    //当客户端通过连接发来数据时(Workerman收到数据时)触发的回调函数
    public function onMessage($connection, $task_data)
    {
        //处理异步任务
        $task_data = json_decode($task_data, true);


        //开启协程
        go(function () use ($connection,$task_data) {
            var_dump('888');
//            switch ($task_data['type'])
//            {
//                case "":
//                    break;
//
//                default:
//            }
        });

        $connection->send('success ok!');
    }


    //当客户端连接与Workerman断开时触发的回调函数。
    public function onClose($connection)
    {
//        var_dump('task_Close');
    }


    //当客户端的连接上发生错误时触发。
    public function onError($connection)
    {
//        var_dump('task_Error');
    }
}