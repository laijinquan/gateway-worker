<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/29
 * Time: 10:13
 */
namespace app;

// 自动加载类
require_once __DIR__ . '/../../vendor/autoload.php';

class Mysql
{
    private $host='127.0.0.1';      //主机id
    private $port='3306';           //端口号
    private $user='root';           //用户
    private $psw='root';            //密码
    private $dbname='layton';       //数据库名称
    private $charset='utf8';        //字符集

    //数据库连接句柄
    private $Db;
    //定义静态变量保存当前类的实例
    private static $instance;

    /**
     * Description:静态方法，单例访问统一入口
     * @return Singleton：返回应用中的唯一对象实例
     */
    public static function GetInstance()
    {
        //当前对象不属于当前例就实例化，也就是静态变量在当前类中只能实例化一次，若是第一次实例化就实例化，若第二次实例化就返回一个当前的实例值。
        if (!(self::$instance instanceof self))
        {
            self::$instance = new static();
        }
        return self::$instance;
    }

    /**
     * constructor:防止在外部实例化
     */
    private function __construct()
    {
        $this->Db = new \Workerman\MySQL\Connection($this->host,$this->port,$this->user,$this->psw,$this->dbname,$this->charset);
    }


    /**
     * constructor:防止在外部克隆
     */
    private function __clone(){}


    /**
     * 获取连接数据库的实例
     * @return WorkermanMySQL
     */
    public function getMySqlDb()
    {
        return $this->Db;
    }

    /**
     * 封装一个执行SQL语句的方法
     * @param $sql
     * @return mixed
     */
    public function query($sql)
    {
        $sqlRes = $this->Db->query($sql);
        return $sqlRes;
    }

}