<?php
/**
 * 用于处理消息队列
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/29
 * Time: 17:24
 */

// 自动加载类
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__.'/Queue.php';
require_once __DIR__.'/File.php';

use \Workerman\Worker;
use \Workerman\WebServer;

// task worker，使用Text协议
$queue_worker = new Worker('http://0.0.0.0:9503');
// task进程数可以根据需要多开一些
$queue_worker->count = 2;
$queue_worker->name = 'queue';

//设置日志文件
$date = date('Y-m-d',time());
$path = __DIR__."/../runtime/start$date.log";
app\File::createFileLog($path);
Worker::$logFile = $path;


// 创建一个对象
$queue = new \app\Queue();

// 调用类的方法
$queue_worker->onWorkerStart= array($queue, 'onWorkerStart');
$queue_worker->onConnect    = array($queue, 'onConnect');
$queue_worker->onMessage    = array($queue, 'onMessage');
$queue_worker->onClose      = array($queue, 'onClose');
$queue_worker->onError      = array($queue, 'onError');



// 如果不是在根目录启动，则运行runAll方法
//if(!defined('GLOBAL_START'))
//{
//    Worker::runAll();
//}
