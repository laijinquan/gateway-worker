<?php
namespace app;

class File
{
    //创建文件
    public static function createFileLog($path)
    {
        if (is_dir($path)) {
            return false;
        }

        $files = is_file($path);
        if ($files)
        {
            return false;
        }

        $myfile = fopen($path, "a");
        fclose($myfile);
    }
}