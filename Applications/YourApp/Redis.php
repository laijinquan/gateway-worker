<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/30
 * Time: 10:10
 */
namespace app;
require_once __DIR__ . '/../../vendor/autoload.php';
use Workerman\Redis\Client;

class Redis
{
    private $host='127.0.0.1';      //主机id
    private $port='6379';           //端口号
    private $psw='';                //密码

    //数据库连接句柄
    private $redis;
    //定义静态变量保存当前类的实例
    private static $instance;

    /**
     * Description:静态方法，单例访问统一入口
     * @return Singleton：返回应用中的唯一对象实例
     */
    public static function GetInstance()
    {
        //当前对象不属于当前例就实例化，也就是静态变量在当前类中只能实例化一次，若是第一次实例化就实例化，若第二次实例化就返回一个当前的实例值。
        if (!(self::$instance instanceof self))
        {
            self::$instance = new static();
        }
        return self::$instance;
    }

    /**
     * constructor:防止在外部实例化
     */
    private function __construct()
    {

        $url = 'redis://'.$this->host.':'.$this->port;
        $redis = new Client($url);
        //密码
        if ($this->psw)
        {
            $redis->auth($this->psw, function ($result) {
                if (!$result)
                {
                    echo "Redis Connection error\n";
                }
            });
        }

        echo "Redis Connection success\n";
        $this->redis = $redis;

    }


    /**
     * constructor:防止在外部克隆
     */
    private function __clone(){}


    public function getRedis()
    {
        return $this->redis;
    }
}